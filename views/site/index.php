  
 <?php
      use yii\grid\GridView;


    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'texto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

