﻿USE ejemplo3yiidesarrollo;


CREATE TABLE noticias(

  id int AUTO_INCREMENT PRIMARY KEY,
  titulo varchar(50),
  texto varchar(200)
);


INSERT INTO noticias VALUES (1,'La estación de autobús de Santander','La estación de autobús de Santander luce "condiciones dignas" tras la reforma

Este viernes se ha inaugurado la reforma de la estación de autobuses de Santander tras una inversión cercana al millón de euros en esta terminal"' ),
  (2,'El polideportivo de Villasevil se abrirá en marzo','El vicepresidente del Gobierno de Cantabria, Pablo Zuloaga, ha visitado hoy las obras del nuevo pabellón polideportivo de Villasevil');
